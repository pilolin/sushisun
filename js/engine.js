document.addEventListener('DOMContentLoaded', function() {
    var observer = lozad('.lazy', {
        loaded: function(el) {
            if(el.closest('.product-item__img')) {
                el.closest('.product-item__img').classList.add('loaded');
            } else {
                el.classList.add('loaded');
            }
        }
    });
    observer.observe();
})

MicroModal.init({
    openTrigger: 'data-open-modal',
    closeTrigger: 'data-close-modal',
    disableScroll: true,
    awaitCloseAnimation: true,
});

// MMENU
var elementMobileMenu = document.querySelector('#mobile-menu');

if (window.innerWidth < 1200 && elementMobileMenu) {
    var menu = new MmenuLight(elementMobileMenu, 'all');

    menu.navigation({ title: 'Меню' });

    var drawer = menu.offcanvas();

    //  Open the menu.
    document.querySelector('a[href="#mobile-menu"]')
        .addEventListener('click', function(evnt) {
            evnt.preventDefault();
            drawer.open();
        });
}

// Фиксированная шапка при прокрутке
window.addEventListener('scroll', function() {
    if (window.matchMedia('(min-width: 992px)').matches) {
        var header = document.querySelector('.header');

        if (window.scrollY) {
            header.classList.add('header-fixed');
        } else {
            header.classList.remove('header-fixed');
        }
    }
});

window.addEventListener('resize', function() {
    if (window.matchMedia('(max-width: 992px)').matches) {
        document.querySelector('.header').classList.remove('header-fixed');
    }
});

new Swiper('.banner .swiper-container', {
    loop: true,
    pagination: {
        el: '.banner .swiper-pagination',
        clickable: true,
    },
});

new Swiper('.establishments-slider .swiper-container', {
    loop: true,
    slidesPerView: 1,
    spaceBetween: 30,
    autoplay: {
        delay: 4000,
    },
    pagination: {
        el: '.establishments-slider .swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        769: { slidesPerView: 2 },
        1201: { slidesPerView: 3 },
    }
});

new Swiper('.feedbacks-slider .swiper-container', {
    loop: true,
    autoplay: {
        delay: 4000,
    },
    pagination: {
        el: '.feedbacks-slider .swiper-pagination',
        clickable: true,
    },
});

// 'Добавить в корзину'
document.addEventListener('click', function(e) {
    if(!e.target.closest('.product-item__bottom--btn .btn')) return;

    var btnAdd = e.target;
    var quantitySection = btnAdd.nextElementSibling;

    btnAdd.setAttribute('aria-hidden', 'true');
    quantitySection.setAttribute('aria-hidden', 'false');
});

function FloatLabel() {
    function handleChange(e) {
        if(e.target.value) {
            e.target.parentNode.classList.add('active');
        }
    }

    function handleFocus(e) {
        e.target.parentNode.classList.add('active');
    }

    function handleBlur(e) {
        var target = e.target;

        if (!target.value || target.tagName.toLocaleLowerCase() === 'select') {
            target.parentNode.classList.remove('active');
        }
    }

    function bindEvents(element) {
        element.addEventListener('change', handleChange);
        element.addEventListener('focus', handleFocus);
        element.addEventListener('blur', handleBlur);
    }

    function init() {
        var floatContainers = document.querySelectorAll('.form-item');

        floatContainers.forEach(function(element) {
            var field = element.querySelector('input, textarea, select');

            if (field) {
                if(field.value) {
                    element.classList.add('active');
                }

                bindEvents(field);
            }
        });
    }

    return {
        init: init
    }
}

FloatLabel().init();

Inputmask({mask: '+7 (999) 999-99-99'}).mask('[type="tel"]');

Inputmask({mask: '9 9 9 9'}).mask('.code-mask');

// Меню в футере на мобильном
document.addEventListener('click', function(e) {
    if(!e.target.closest('.footer-links__item .h5')) return;
    if (window.matchMedia('(min-width: 769px)').matches) return;

    var list = e.target.closest('.footer-links__item .h5').nextElementSibling;
    var isOpened = list.clientHeight;

    animate({
        draw: function(progress) {
            list.style.maxHeight = isOpened
                ? `${(1 - progress) * list.scrollHeight}px`
                : `${(progress) * list.scrollHeight}px`;
        },
        onEnd: function() {
            list.style.maxHeight = isOpened ? '0' : '100%';
        },
    });
});

M.Datepicker.init(document.getElementById('datepicker'));
M.Timepicker.init(document.getElementById('timepicker'));

// Табы
document.addEventListener('click', function(e) {
    if(!e.target.closest('.tabs-nav a')) return;

    var targetTab = e.target.closest('.tabs-nav a');
    var targetContent = document.querySelector(targetTab.hash);

    var tabsContainer = targetTab.closest('.tabs-wrap');

    var activeTab = tabsContainer.querySelector('a[aria-selected="true"]');
    var activeContent = tabsContainer.querySelector('.tabs-item[aria-hidden="false"]');

    activeTab.setAttribute('aria-selected', 'false');
    activeContent.setAttribute('aria-hidden', 'true');

    targetTab.setAttribute('aria-selected', 'true');
    targetContent.setAttribute('aria-hidden', 'false');

    e.preventDefault();
});

// dropdown
document.addEventListener('click', openTargetDropdown);

function openTargetDropdown(event) {
    if(!event.target.closest('.dropdown__header')) return;

    if(event.target.nodeName.toLocaleLowerCase() === 'button') return;

    var targetDropdown = event.target.closest('.dropdown');
    var isExpandedTargetDropdown = targetDropdown.getAttribute('aria-expanded') === 'true';
    var group = targetDropdown.dataset.group;

    closeActiveDropdown(group);

    if(isExpandedTargetDropdown) return;

    var targetDropdownContent = targetDropdown.querySelector('.dropdown__body');
    var originalHeightBody = targetDropdownContent.scrollHeight;

    animate({
        draw: function(progress) {
            targetDropdownContent.style.maxHeight = `${progress * originalHeightBody}px`;
        },
        onStart: function() {
            targetDropdown.setAttribute('aria-expanded', 'true');
        },
        onEnd: function() {
            targetDropdownContent.style.maxHeight = '100%';
        },
    });
}

function closeActiveDropdown(group) {
    var expandedDropdown = group
        ? document.querySelector('.dropdown[aria-expanded="true"][data-group="' + group + '"]')
        : document.querySelector('.dropdown[aria-expanded="true"]');

    if (expandedDropdown) {
        var expandedDropdownContent = expandedDropdown.querySelector('.dropdown__body');
        var originalHeightBody = expandedDropdownContent.scrollHeight;

        expandedDropdownContent.style.maxHeight = originalHeightBody;

        animate({
            draw: function(progress) {
                expandedDropdownContent.style.maxHeight = `${(1 - progress) * originalHeightBody}px`;
            },
            onStart: function() {
                expandedDropdown.setAttribute('aria-expanded', 'false');
            },
        });
    }
}

// анимация js
function animate(opts) {
    var options = Object.assign({
        timing(timeFraction) {
            return timeFraction;
        },
        draw: null,
        duration: 250,
        onStart: function () { },
        onEnd: function () { }
    }, opts)

    options.onStart();

    var start = performance.now();

    requestAnimationFrame(function animate(time) {
        var timeFraction = (time - start) / options.duration;
        if (timeFraction > 1) timeFraction = 1;

        var progress = options.timing(timeFraction);

        options.draw(progress);

        if (timeFraction < 1) {
            requestAnimationFrame(animate);
        } else {
            options.onEnd();
        }
    });
}